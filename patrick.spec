Name: patrick
version: 0.0.0
Release: 0
Summary: example rpm package
License: GPL
BuildArch: noarch

%description
patrick is just my hello world rpm package example

%prep
echo 'prep'

%build
echo 'build'

%install
echo 'install'
ls /root/rpmbuild
cp -rf %{_builddir}/* $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
ls $RPM_BUILD_ROOT/usr/bin
echo $RPM_BUILD_ROOT

%clean
echo 'clean'

%files
%defattr(755,root,root)
/usr/bin/*
