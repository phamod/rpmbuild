FROM centos

RUN yum -y install rpm-build
RUN mkdir -p /root/rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
RUN ls /root/rpmbuild

COPY patrick.spec /
RUN mkdir -p /root/rpmbuild/BUILD/usr/bin
COPY test.sh /root/rpmbuild/BUILD/usr/bin
RUN rpmbuild -ba patrick.spec
RUN cp -rf /root/rpmbuild/RPMS/* /tmp